# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

############################# End Ubuntu boilerplate ###########################

# load custom xmodmap if it exists (e.g. little lappy switch PgUp with Home
#  and PgDn with End)
if [ -f ~/.xmodmap ]; then
    xmodmap ~/.xmodmap
fi

# env vars

## Voir/Workspace build variables
# export VOIRHOME=$HOME/Voir/Voir
# export C2LIAR_HOME=$HOME/Voir/C2LIAR.mkIII
# export ALIAS_HOME=$HOME/Voir/ALIAS
# export BIPLUGIN_HOME=$HOME/Workspace/trunk/BIplugin
# export CMAKE_HOME=$HOME/Voir/CMakeModules
# export WORKSPACE_HOME=/opt/csiro.au/CSIRO-CMIS-CFD/workspace
# export MYZDIR=Linux

## R
#export R_LIBS_USER=$HOME/R/library
#export R_LIBS_USER=$HOME/R/x86_64-pc-linux-gnu-library
#export MYRLIB=$R_LIBS_USER
export RINCLUDE=/usr/lib/R/include

# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

export EDITOR=vim

## misc. path edits
# Checks if the argument exists and is a directory and prepends it to PATH.
# http://superuser.com/questions/39751/add-directory-to-path-if-its-not-already-there
pathadd() {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="$1"${PATH:+":$PATH"}
    fi
}
pathadd $HOME/bin
pathadd $HOME/local/bin/spharm-pdm_v1.11_linux64
pathadd $HOME/.cabal/bin
pathadd $HOME/local/bin
pathadd $HOME/anaconda/bin
pathadd $HOME/.rvm/bin
# texlive
pathadd /usr/local/texlive/2014/bin/x86_64-linux
pathadd /usr/local/texlive/2015/bin/x86_64-linux
pathadd /windows/texlive/2014/bin/x86_64-linux

if [ -d /windows/texlive/2014/texmf-dist/doc/info ] ; then
    INFOPATH=/windows/texlive/2014/texmf-dist/doc/info${INFOPATH:+":$INFOPATH"}
fi
if [ -d /windows/texlive/2014/texmf-dist/doc/man ] ; then
    MANPATH=/windows/texlive/2014/texmf-dist/doc/man${MANPATH:+":$MANPATH"}
fi

if [ -d $HOME/local/lib/python2.7/site-packages ]; then
    export PYTHONPATH=${PYTHONPATH:+$PYTHONPATH:}$HOME/local/lib/python2.7/site-packages
fi
if [ -d /usr/local/src/caffe/build/install/python ]; then
    export PYTHONPATH=${PYTHONPATH:+$PYTHONPATH:}/usr/local/src/caffe/build/install/python
fi

# startup programs to run (chrome, thunderbird) --gnome session manager can't remember them
# Note: I think that was a script with commands to launch chrome etc.
if [ -f ~/.startup ]; then
    . ~/.startup
fi

# line wrap git log -n 1
export LESS=-FRX

# for crontab to change my wallpaper with randomWallpaperChanger.pl
if [ "$DISPLAY" != "" ] ; then
    xhost local:$USER > /dev/null;
fi

## Freesurfer
if [ -d /usr/local/freesurfer ] ; then
    export FS_FREESURFERENV_NO_OUTPUT=1
    export FREESURFER_HOME=/usr/local/freesurfer
    source $FREESURFER_HOME/SetUpFreeSurfer.sh
    # $SUBJECTS_DIR: where the subjects are.
fi
if [ -d $HOME/local/src/freesurfer ] ; then
    export FS_FREESURFERENV_NO_OUTPUT=1
    export FREESURFER_HOME=$HOME/local/src/freesurfer
    source $FREESURFER_HOME/SetUpFreeSurfer.sh
    # $SUBJECTS_DIR: where the subjects are.
fi

# have a random cow say a fortune, if the user has both fortune & cowsay installed.
# (TODO: maintain the nhqdb as fortunes)
type fortune &> /dev/null && type cowsay &> /dev/null && (fortune | cowsay -f $(ls /usr/share/cowsay/cows/ | shuf -n1))

# Lines added by the Vim-R-plugin command :RpluginConfig (2014-Jul-28 13:47):
# Change the TERM environment variable (to get 256 colors) and make Vim
# connecting to X Server even if running in a terminal emulator (to get
# dynamic update of syntax highlight and Object Browser):
if [ "x$DISPLAY" != "x" ]
then
    export HAS_256_COLORS=yes
    alias tmux="tmux -2"
    if [ "$TERM" = "xterm" ]
    then
        export TERM=xterm-256color
    fi
    alias vim="vim --servername VIM"
    if [ "$TERM" == "xterm" ] || [ "$TERM" == "xterm-256color" ]
    then
        function tvim(){ tmux -2 new-session "TERM=screen-256color vim --servername VIM $@" ; }
    else
        function tvim(){ tmux new-session "vim --servername VIM $@" ; }
    fi
else
    if [ "$TERM" == "xterm" ] || [ "$TERM" == "xterm-256color" ]
    then
        export HAS_256_COLORS=yes
        alias tmux="tmux -2"
        function tvim(){ tmux -2 new-session "TERM=screen-256color vim $@" ; }
    else
        function tvim(){ tmux new-session "vim $@" ; }
    fi
fi
if [ "$TERM" = "screen" ] && [ "$HAS_256_COLORS" = "yes" ]
then
    export TERM=screen-256color
fi

