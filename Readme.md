A collection of my `.*` files for ease of use.

* `.vimrc` and `.vim` folder
    + for `vim-pandoc-syntax` and `vim-pantondoc`, you need to compile vim with `--with-features=big` to get `+conceal` and you need vim 7.4.
* `.nethackrc` (onwards nethacker!)
* `.bashrc` with `.bash_aliases`
* `.ackrc` for `ack-grep`
* `.hgrc` for mercurial
* `.gitconfig` for git
* `.Rprofile` (at the moment just a couple of config for RStudio/pandoc)

```
sudo apt-get install git
cd ~
git clone git@bitbucket.org:mathematicalcoffee/dotfiles.git

# from dotfiles directory
for f in ackrc bash_aliases bashrc gitconfig hgrc nethackrc Rprofile tmux.conf vim vimrc; do
    ln -s dotfiles/$f ~/.$f;
    # or ln -s ~/dotfiles/$f ~/.$f
done
git submodule init
git submodule update
```

# New computer setup

Before setting up dotfiles above (well it doesn't really matter but some plugins are incompatible until you do the setup)

```
# stuff I always install
sudo apt-get install git hg ack-grep guake compizconfig-settings-manager nethack-console imagemagick cabal-install gnome-shell gnome-tweak-tool compiz-plugins-extra
```

Edit the terminal keyboard prefs (Shift+{arrow}); guake prefs (similar).

```
ssh-keygen
cat ~/.ssh/id_rsa.pub
```

Copy to github and bitbucket.

* [R](#R)
* [vim](#vim)
    - [vim-r-plugin](#vim-r-plugin) (after doing both R and vim)
* Install dotfiles as above.
* [Zotero](#zotero) and extensions:
    - [Zotfile](#zotfile) for auto-renaming
    - [Zotero Better Bib(La)TeX](#better-bibtex) for better citekey control in the bibtex
    - [Zotero Autoexport](#zotero-autoexport) for automatic bibtex updating

## bashrc

Has cowsay fortune in it somewhere.
Also install `fortunes-bofh-excuses` and `fortunes` for some extra fortunes...

  sudo apt-get install cowsay fortune-mod fortunes fortunes-off fortunes-bofh-excuses fortunes-debian-hints fortunes-mario fortunes-spam fortunes-ubuntu-server 

## R

Prereqs:

```
# need curl-config for Rcurl package
sudo apt-get install libcurl4-openssl-dev libxml2-dev

# add apt repository
echo "deb http://cran.csiro.au/bin/linux/ubuntu trusty/" | sudo tee /etc/apt/sources.list.d/r.list > /dev/null
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
sudo apt-get update
sudo apt-get install r-base-dev
```

in R:

```
# needs to be done first
install.packages(c('codetools', 'MASS'))
# packages I use a lot:
install.packages(c('ggplot2', 'devtools', 'pander', 'knitr', 'stringr', 'testthat', 'roxygen2', 'XML', 'plyr', 'dplyr'))

# what version of datatable do you want?
install.packages('data.table')
# OR
library(devtools)
install_github('data.table', 'Rdatatable')
# install_github('data.table', 'Rdatatable', build_vignettes=F)
```

## Vim

This installs vim 7.4 with X support (for vim-rplugin), conceal (for pantondoc), and pythonX-interp (vim-rplugin requires one of these).

```
hg clone https://vim.googlecode.com/hg/ vim
cd vim/src
# sudo apt-get install libxpm-dev libxt-dev libx11-dev python-dev python3 python3-dev
rm auto/config.cache
./configure --with-features=big --enable-pythoninterp=yes --enable-python3interp=yes --with-x # --prefix=$HOME/local

# version with ncurses in $HOME/
CFLAGS=-I$HOME/include LDFLAGS=-L$HOME/lib ./configure ...

make
make install
```

### Vim-R-plugin

First you need:

```
sudo apt-get install tmux
```

Download <http://www.vim.org/scripts/script.php?script_id=2628> vimball (note - if you install dotfiles you can skip this because it's part of it. But still need to download the R packages)

```
vim Vim-R-plugin.vmb
:so %
```

Then see <http://www.lepem.ufc.br/jaa/r-plugin.html#r-plugin-installation> to install the rest:

```
# vimcom
download.file("http://www.lepem.ufc.br/jaa/vimr/vimcom_1.2-5.tar.gz",
              destfile = "vimcom_1.2-5.tar.gz")
install.packages("vimcom_1.2-5.tar.gz", type = "source",
                  repos = NULL)

# colorout
download.file("http://www.lepem.ufc.br/jaa/vimr/colorout_1.1-0.tar.gz",
              destfile = "colorout_1.1-0.tar.gz")
install.packages("colorout_1.1-0.tar.gz", type = "source", repos = NULL)

# setwidth
install.packages('setwidth')
```

### Tmux

The file `.tmux.config` just makes it so you can copy/paste with the mouse in Tmux.

## Compiz

From my 12.04 computer. Be careful merging with the 14.10 computers.
Preferences -> Export.

* Scale: set window picker to F9, bottom-left corner
* Expo: initiate on F8, bottom-right corner.
* Window rules: resize the 'NAO' gnome-terminal to 80x24 (must modify terminal prefs to set title to NAO for that profile).
* Unity-specific plugins are enabled [by default]
* Enabled the 'resize info' plugin to know what size I'm resizing my windows (in part. terminals since changing profiles doesn't seem to set the size properly)
* Changed keybindings in 'grid' to `<ctrl>+<windows>+<direction on keypad>` to tile in particular corners [also in Ubuntu I have `<ctrl>+<windows>+<arrow>` for resizing in particular directions].
* Static application switcher: enable. Disable the switching keybindings in "Unity plugin"

## Rprofile

I used to have functions to knit via pandoc in RStudio, but now knitr has pandoc support so I don't have to (removed them 2015-09-29).

There's a bit more RStudio farting around:

* add texlive, rvm to the `$PATH` (since RStudio doesn't use my `.bashrc` where I set up my `$PATH`). (rvm for `servr` jekyll serving).
* I like text help, but not in RStudio
* for some reason have recently started to get errors when installing packages in Linux where I never used to before - maybe
  the default method is now curl instead of wget? switched it back to wget.
* import the system fortunes to the local fortunes database (should make it so this is only done once)
* cowsay a fortune (!).

## Zotero

**Update**: I have a [repo with my zotero folder saved](https://bitbucket.org/mathematicalcoffee/zotero). It's set up for PhD stuff so the paper directory is `~/phd/papers`.

This is to setup zotero to sync to a folder of my choice (not the online storage which is only 300MB and has unintuitive naming of files).

```
sudo add-apt-repository ppa:smathot/cogscinl
sudo apt-get update
sudo apt-get install zotero-standalone #qnotero
```

Edit -> Preferences:

* Under 'Sync' -> Settings: can add zotero username/password if you want (though I'm not sure if I end up doing this?)
* Under 'Search' : PDF Indexing: install pdftotext/pdfinfo from here if it needs it (for some reason my linux system already has these but it doesn't detect them).
    - Rebuild Index?
* Advanced -> Files and Folders:
    - Linked Attachment Base Directory: I set it to ~/phd/papers for now (NOTE: set to Copy somewhere or just rely on git for syncing?). Papers get stored here.
    - Data Directory Location: leave as default

Now set up plugins and configure.

### Importing from Mendeley

First time once-off: Make sure the Mendeley `library.bib`'s paths are correct, then File > Import and choose the bibfile to import all the documents.

### Zotfile

Go to <http://zotfile.com/>.

Download the xpi file, go to Tools > Addons > click the gear icon top-left and install from file.

Configure (Tools > Addons > Preferences):

* General Settings > Location of Files: Pick 'custom location' and point to the folder that is your 'linked attachment base directory' (e.g. `~/phd/papers`)
* Renaming Rules: change to whatever you want. e.g. `{%a-}{%y-}{%t}` with space for delimiter between authors, replace blanks, max number of authors 3, split at `[?:.]` for no space names. Or `{%a - }{%y - }{%t}` with `, ` for delimiter between authors, no replace blanks, ` et al.` suffix for ommitted authors to more-or-less match Mendeley.

### Better bibtex

Go to <https://github.com/ZotPlus/zotero-better-bibtex>.
Download the XPI and install.

Edit > Preferences > Better Bib(La)tex to configure (I don't think I did any configuring).

To change the citekey format see [here](https://zotplus.github.io/better-bibtex/citation-keys.html) and [here](http://jabref.sourceforge.net/help/LabelPatterns.php). I use [auth][year].

### Zotero autoexport

Go to <http://rokdd.de/b/zotero-autoexport>.

Configure:

* General > Autoexport mode: by triggers and manually.
* Export > trigger by events: enable "when items are added, removed or updated", uncheck "by timer"
* Export > file format and path:
    - Choose "Better BibTeX" as the fileformat
    - "Path to file": to (e.g.) `~/phd/papers/library.bib`.

**Note** 8 Oct 2015: the latest version in Mozilla is not the latest version, if you have zotero 4+ you need to get autoexport 1.2+ or else it doesn't export. Go to the website and download his version from there, rather than through mozilla.

### Note on relative paths

The `file` field of the bibtex says (e.g.)

```
 file = {Ziegler_Dahnke_Gaser-2012-Models_of_the_aging_brain_structure_and_individual_decline.pdf:/home/cha66i/papers/Ziegler_Dahnke_Gaser-2012-Models_of_the_aging_brain_structure_and_individual_decline.pdf:application/pdf}
```

That is, `{filename:absolute/path/to/filename:application/pdf}`.

AFAIK there is **no way** to have the absolute path be relative to the papers directory in the way that I want.
The best you get is:

When you export, click 'export files'; zotero will create a folder with a bib file in it, as well as a subfolder `files` with all the papers in it. The path in the bibtex will be relative to the bibtex file (something file `files/number/pdf-name.pdf`).

You can go to Preferences > Advanced > 'Open about:config' and search for `relative`. The key `better-bibtex.attachmentRelativePath` can be set to `true` and then the bibtex will have this `files/number/pdf-name.pdf` relative path, but said folder won't exist.

So you can't (as far as I know) set the path relative to some arbitrary path.

## Chrome

https://www.google.com/chrome/browser/desktop/

It will add the repos for you.

## Guake

If you find it doesn't get key focus when you open it (unless you open it twice in a row), see [#216](https://github.com/Guake/guake/issues/216). Go to ccsm > General > Focus & raise behaviour > set 'Focus prevention level' to 'off'

## Volume control

If you want the volume buttons to have a smaller/finer increment/decrement, use [this solution](http://askubuntu.com/a/215886/36279), reproduced below.

> You will not have on-screen volume bar action when you use the keyboard shortcuts, but you will have however fine-grained volume control as you wish.
>
> 1. System Settings > Keyboard > "Shortcuts" tab > "Sound and Media" category
> 2. Disable the existing "Volume Down" and "Volume Up" shortcuts. To do this, on each one click to select it and then press Backspace to clear any key combo associated with it.
> 3. Now select the "Custom Shortcuts" category and click the "+" icon to create two new shortcuts as follows:
> 
>        Name: Volume Up
>        Command: amixer set Master 3%+
>
>        Name: Volume Down
>        Command: amixer set Master 3%-
> 
>    (Experiment with the percentages. If you need to go extremely fine then omit the % sign and it will use a 0-255 scale rather than percent).
> 4. Now assign each of your new shortcuts to a key or key combo: Select a shortcut and type the desired key or keys on your keyboard.
>
> After this, when you use your keyboard volume controls you should have whatever volume increments you specified. You can always go back to the original behavior by disabling your custom shortcuts and re-enabling the premade ones in the "Sound and Media" category.

## Ruby

Use RVM

  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
  \curl -sSL https://get.rvm.io | bash -s stable
  rvm install 2.1.1
  # In terminal: Edit -> Profile Preferences -> Title and Command -> Run Command as login shell
  rvm use --default 2.1.1 # e.g.
  rvm docs generate-ri
  gem install bundler
  bundle install

### Obsolete

The version of ruby used by jekyll for github-pages is ruby2.0, but installing ruby2 does not set the default `/usr/bin/ruby` to ruby2.0.
See [this response](https://bugs.launchpad.net/ubuntu/+source/ruby2.0/+bug/1310292/comments/25) (stevenschlansker):

> Here is our workaround which should be better than the ones proposed above (doesn't get wiped out by package upgrade / reinstall)
>
>     # Rename original out of the way, so updates / reinstalls don't squash our hack fix
>     sudo dpkg-divert --add --rename --divert /usr/bin/ruby.divert /usr/bin/ruby
>     sudo dpkg-divert --add --rename --divert /usr/bin/gem.divert /usr/bin/gem
>     # Create an alternatives entry pointing ruby -> ruby2.0
>     sudo update-alternatives --install /usr/bin/ruby ruby /usr/bin/ruby2.0 1
>     sudo update-alternatives --install /usr/bin/gem gem /usr/bin/gem2.0 1

Do the same for `irb`

    sudo dpkg-divert --add --rename --divert /usr/bin/irb.divert /usr/bin/irb
    sudo update-alternatives --install /usr/bin/irb irb /usr/bin/irb2.0 1

## bluetooth

On grug in `/etc/rc.local`:

    rfkill block bluetooth

To disable it by default. But if you want to use bluetooth you have to `sudo rfkill unblock bluetooth`.
I'd prefer it to be enabled, but off, by default. However this doesn't seem to work.

## Touchpads

If it's super sensitive and your palm is activating it while you type, here are some options:

First, `syndaemon` (http://www.webupd8.org/2009/11/ubuntu-automatically-disable-touchpad.html) which can disable the touchpad while you're typing and re-enable it after some delay.

> The following example invokes `syndaemon` for 4 seconds after any keyboard activity (`-i 4`), except when modifier keys such as Alt or Shift are used (`-K`), and only disables tapping and scrolling (`-t`) for this period:
> 
>     syndaemon -i 4 -d -t -K

Secondly, can increase the palm sensitivity to stop it being interpreted as a touch: http://askubuntu.com/questions/205512/touchpad-palm-sensitivity.

    synclient -l | grep -i palm # what options are available
    synclient PalmDetect=1 PalmMinWidth=xx PalmMinZ=yy

> For my ASUS S-200, which has an ETPS/2 Elantech Touchpad, I found good settings were PalmMinWidth=5 and PalmMinZ=20. YPMV. (Your palms may vary) –  ngm Jul 21 '13 at 9:52

## LaTeX

I use TeXLive from the internet (`tlmgr`) rather than the ubuntu repos. http://tug.org/texlive/acquire-netinstall.html
I install as root.

Download the install.tar.gz from above, untar (mine was not gzipped despite file extension), `cd` to the dir, `sudo ./install-tl`.

You need heaps of GB to install.
