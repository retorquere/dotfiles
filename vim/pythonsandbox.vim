" 'echo' and 'silent' ignored.
" PySourceLines -->  SendFileToR, SendSelectionToR  
"    maybe should change RSourceLines to do a 'if &filetype=="py"'?
"    will syntax highlighting be funny?
"    need to enable the plugin for .py files too, so syntax highlighting will
"    be funny....
" file, line, selection.
call RCreateMaps("ni", '<Plug>RSendFile',     'aa', ':call SendFileToR("silent")')
call RCreateMaps("ni0", '<Plug>RSendLine', 'l', ':call SendLineToR("stay")')
call RCreateMaps("v", '<Plug>RESendSelection',  'se', ':call SendSelectionToR("echo", "stay")')

" Send sources to Py
function PySourceLines(lines, e)
    call writefile(a:lines, b:rsource)
    " for now disregard a:e
    let rcmd = 'execfile("' . b:rsource . '")'
    let ok = SendCmdToR(rcmd)
    return ok
endfunction

" Send file to Py
function SendFileToPy(e)
    echon
    let b:needsnewomnilist = 1
    let lines = getline("1", line("$"))
    let ok = PySourceLines(lines, a:e)
    if  ok == 0
        return
    endif
endfunction

" Send selection to R
function SendSelectionToPy(e, m)
    echon
    let b:needsnewomnilist = 1
    if line("'<") == line("'>")
        let i = col("'<") - 1
        let j = col("'>") - i
        let l = getline("'<")
        let line = strpart(l, i, j)
        let ok = SendCmdToR(line)
        if ok && a:m =~ "down"
            call GoDown()
        endif
        return
    endif
    let lines = getline("'<", "'>")
    let ok = PySourceLines(lines, a:e) " <----
    if ok == 0
        return
    endif
    if a:m == "down"
        call GoDown()
    else
        normal! gv
    endif
endfunction

""""""""""""""""""""""""""""""""
function RWarningMsg(wmsg)
    echohl WarningMsg
    echomsg a:wmsg
    echohl Normal
endfunction

" Function to send commands
" return 0 on failure and 1 on success
function SendCmdToR(cmd)
    " ^K clean from cursor to the right and ^U clean from cursor to the left
    let cmd = "\013" . "\025" . a:cmd

    if g:vimrplugin_screenplugin
        if !exists("g:ScreenShellSend")
            call RWarningMsg("Did you already start Python?")
            return 0
        endif
        call g:ScreenShellSend(cmd)
        return 1
    elseif g:vimrplugin_conqueplugin
        if !exists("b:conque_bufname")
            if g:vimrplugin_by_vim_instance
                if exists("g:rplugin_conqueshell")
                    let b:conqueshell = g:rplugin_conqueshell
                    let b:conque_bufname = g:rplugin_conque_bufname
                    let b:objbrtitle = g:rplugin_objbrtitle
                else
                    call RWarningMsg("This buffer does not have a Conque Shell yet.")
                    return 0
                endif
            else
                call RWarningMsg("Did you already start Python?")
                return 0
            endif
        endif

        " Is the Conque buffer hidden or deleted?
        if !bufloaded(substitute(b:conque_bufname, "\\", "", "g"))
            call RWarningMsg("Could not find Conque Shell buffer.")
            return 0
        endif

        " Code provided by Nico Raffo: use an aggressive sb option
        let savesb = &switchbuf
        set switchbuf=useopen,usetab

        " jump to terminal buffer
        if bufwinnr(substitute(b:conque_bufname, "\\", "", "g")) < 0
            " The buffer either was hidden by the user with the  :q  command or is
            " in another tab
            exe "sil noautocmd belowright split " . b:conque_bufname
        else
            exe "sil noautocmd sb " . b:conque_bufname
        endif

        " write variable content to terminal
        call b:conqueshell.writeln(cmd)
        exe "sleep " . g:vimrplugin_conquesleep . "m"
        call b:conqueshell.read(50)
        normal! G0

        " jump back to code buffer
        exe "sil noautocmd sb " . g:rplugin_curbuf
        exe "set switchbuf=" . savesb
        return 1
    endif

    " Send the command to R running in an external terminal emulator
    let str = substitute(cmd, "'", "'\\\\''", "g")
    let scmd = 'screen -S ' . b:screensname . " -X stuff '" . str . "\<C-M>'"
    let rlog = system(scmd)
    if v:shell_error
        let rlog = substitute(rlog, '\n', ' ', 'g')
        let rlog = substitute(rlog, '\r', ' ', 'g')
        call RWarningMsg(rlog)
        return 0
    endif
    return 1
endfunction

" Send current line to R. Don't go down if called by <S-Enter>.
function SendLineToR(godown)
    echon
    let line = getline(".")

    let b:needsnewomnilist = 1
    let ok = SendCmdToR(line)
    if ok
        if a:godown =~ "down"
            call GoDown()
        else
            if a:godown == "newline"
                normal! o
            endif
        endif
    endif
endfunction

" Send sources to Py
function RSourceLines(lines, e)
    call writefile(a:lines, b:rsource)
    " for now disregard a:e
    let rcmd = 'execfile("' . b:rsource . '")'
    let ok = SendCmdToR(rcmd)
    return ok
endfunction

" Send file to Py
function SendFileToR(e)
    echon
    let b:needsnewomnilist = 1
    let lines = getline("1", line("$"))
    let ok = RSourceLines(lines, a:e)
    if  ok == 0
        return
    endif
endfunction

" Send selection to R
function SendSelectionToPy(e, m)
    echon
    let b:needsnewomnilist = 1
    if line("'<") == line("'>")
        let i = col("'<") - 1
        let j = col("'>") - i
        let l = getline("'<")
        let line = strpart(l, i, j)
        let ok = SendCmdToR(line)
        if ok && a:m =~ "down"
            call GoDown()
        endif
        return
    endif
    let lines = getline("'<", "'>")
    let ok = RSourceLines(lines, a:e) " <----
    if ok == 0
        return
    endif
    if a:m == "down"
        call GoDown()
    else
        normal! gv
    endif
endfunction

" For each noremap we need a vnoremap including <Esc> before the :call,
" otherwise vim will call the function as many times as the number of selected
" lines. If we put the <Esc> in the noremap, vim will bell.
" RCreateMaps Args:
"   type : modes to which create maps (normal, visual and insert) and whether
"          the cursor have to go the beginning of the line
"   plug : the <Plug>Name
"   combo: the combination of letter that make the shortcut
"   target: the command or function to be called
function RCreateMaps(type, plug, combo, target)
    if a:type =~ '0'
        let tg = a:target . '<CR>0'
        let il = 'i'
    else
        let tg = a:target . '<CR>'
        let il = 'a'
    endif
    if a:type =~ "n"
        if hasmapto(a:plug, "n")
            exec 'noremap <buffer> ' . a:plug . ' ' . tg
        else
            exec 'noremap <buffer> <LocalLeader>' . a:combo . ' ' . tg
        endif
    endif
    if a:type =~ "v"
        if hasmapto(a:plug, "v")
            exec 'vnoremap <buffer> ' . a:plug . ' <Esc>' . tg
        else
            exec 'vnoremap <buffer> <LocalLeader>' . a:combo . ' <Esc>' . tg
        endif
    endif
    if a:type =~ "i"
        if hasmapto(a:plug, "i")
            exec 'inoremap <buffer> ' . a:plug . ' <Esc>' . tg . il
        else
            exec 'inoremap <buffer> <LocalLeader>' . a:combo . ' <Esc>' . tg . il
        endif
    endif
endfunction
