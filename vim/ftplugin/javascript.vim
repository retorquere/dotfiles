" GJS lint
" (TODO: set up with jslint as well)
setlocal colorcolumn=80

" ERROR CHECKING: the following runs jshint and gjslint on the current buffer,
" opening a list of errors.
" Use C-n for the next error & C-p for the previous error (navigation)
" Use either :GJScheck or <F5> to do it. (in vis, normal *or* insert mode...)
" GJSlint:
" command! GJScheck call DoMake('gjslint --nosummary --unix_mode --nobeep', 'jshint')
command! GJScheck call DoMake('jshint')
function! DoMake(...)
  update  " save any changes because makeprg checks the file on disk
  let savemp = &makeprg
  let qflist = []
  for prg in a:000
    let &makeprg = prg . ' %'
    silent make!
    let qflist += getqflist()
  endfor
  if empty(qflist)
    cclose
  else
    call setqflist(qflist)
    copen
    cfirst
  endif
  let &makeprg = savemp
endfunction

nmap <F5> :GJScheck<cr>
imap <F5> <esc>:GJScheck<cr>
vmap <F5> <esc><esc>:GJScheck<cr>

" TODO: set options in local .config file (I guess you do them on a
" file-by-file bases with the thing up the top)
