" PEP-8: wrap @ 79 columns, wrap comments @ 72 columns
setlocal colorcolumn=72,79

" folding is indent-based
setlocal foldmethod=indent

" disable showing marks in the left border
let g:showmarks_enable=0

" smart indenting
setlocal smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class

" Auto completion via ctrl-space
setlocal omnifunc=pythoncomplete#Complete
inoremap <Nul> <C-x><C-o>

" If you get this plugin http://www.vim.org/scripts/script.php?script_id=1112,
" pressing 'shift-k' takes you to documentation for the word under the cursor
" Also, :PyDoc <modulename>.

" Execute a selection of code (very cool!)
" Use VISUAL to select a range and then hit ctrl-h to execute it.
python << EOL
import vim
def EvaluateCurrentRange():
    eval(compile('\n'.join(vim.current.range),'','exec'),globals())
EOL
map <C-h> :py EvaluateCurrentRange()


" Let :make map to pylint so you can use :co to open the error buffer,
" Ctrl-n (:cn) to go to the next error, Ctrl-p (:cp) to go the previous error
"set makeprg=pylint\ --reports=n\ --include-ids=y\ --output-format=parseable\ %:p
"set errorformat=%f:%l:\ %m
" what about pep8?
" pep8
"set makeprg=pep8\ %:p
"set errorformat=%f:%p:%c:\ %m

" ERROR CHECKING: the following runs pylint and pep8 on the current buffer,
" opening a list of errors.
" Use C-n for the next error & C-p for the previous error (navigation)
" Use either :Pycheck or <F5> to do it. (in vis, normal *or* insert mode...)
command! Pycheck call DoMake('pylint --reports=n --include-ids=y --output=parseable --variable-rgx=''[a-z_][a-z0-9_]{0,30}'' --argument-rgx=''([a-z]\|[a-z_][a-zA-Z0-9_]{1,30})''', 'pep8')
function! DoMake(...)
  update  " save any changes because makeprg checks the file on disk
  let savemp = &makeprg
  let qflist = []
  for prg in a:000
    let &makeprg = prg . ' %'
    silent make!
    let qflist += getqflist()
  endfor
  if empty(qflist)
    cclose
  else
    call setqflist(qflist)
    copen
    cfirst
  endif
  let &makeprg = savemp
endfunction

nmap <F5> :Pycheck<cr>
imap <F5> <esc>:Pycheck<cr>
vmap <F5> <esc><esc>:Pycheck<cr>
