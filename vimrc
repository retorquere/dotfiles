" My changes
" tab == 4 spaces
set tabstop    =4
set shiftwidth =4
set sw         =4
set expandtab

set history=50      " keep 50 lines of command line history (default 20)
set ruler           " show the cursor position all the time
set showcmd         " display incomplete commands
set incsearch       " do incremental searching

set nocompatible " vi-compatible
syntax enable
filetype plugin on
filetype plugin indent on
filetype indent on

" =========== COLOUR =========== "
" select colour scheme
colorscheme ron
set cc=80
hi ColorColumn ctermbg=DarkGray " black bg for cc so it doesn't stand out so much. or try 8 (grey)


" slow vim on Ruby
" http://stackoverflow.com/questions/16902317/vim-slow-with-ruby-syntax-highlighting
" TODO later: for ruby files only?
set re=1

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
   set mouse=vi "a
endif

" Hmm, should probably only do this for code files: python, R, ...
"autocmd FileType cpp,c,cxx,h,hpp,python,sh,r,perl
autocmd FileType markdown,pandoc,html,ruby,yaml setlocal tabstop=2 shiftwidth=2


" <space> to fold/unfold
nnoremap <space> za

" try synchronise vim's default copy/paste register with the system one
set clipboard^=unnamed

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " Python: see ftplugin/python.vim
  " R: see ftplugin/r.vim

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent        " always set autoindenting on

endif " has("autocmd")

" pressing F3 will toggle between paste mode
set pastetoggle=<F3>

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
 if !exists(":DiffOrig")
   command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
		  \ | wincmd p | diffthis
 endif

" ======== LEADER ======= "
" Used in the showmarks and R plugins. Although ',' is nice it's very common
" and in insert mode slows down a little to see if you meant to type anything 
" else after it :( 
" I changed it to ';' which although I use it often always with a spac after
" it.
let maplocalleader=";"
let mapleader=";"

" =========== FOLDING =========== "
" za toggles folding, zo opens a fold, zc closes a fold.
"set foldmethod=indent  " fold based on indent
set foldmethod=syntax   " fold based on syntax
set foldenable          " folding enabled by default
set foldlevelstart=99   " folds open by default.

" =========== ALIASES =========== "
" do ,E to insert new line before current, ,e for after.
"nmap ,E O<Esc>
"nmap ,e o<Esc>

" -- Backspace, Enter, tab all work in normal mode: (delete already does)
" press Enter to insert new line before cursor
nmap <Enter> i<Enter><Esc>
"nmap <Backspace> "zdh
nmap <Tab> i<Tab><Esc>h 

"restore backspace functionality (wouldn't let me backspace over insertion
"point)
set bs=2 

" do ,. to go to end of sentence and make a new line
nmap ,. )i<Enter><Esc>
" split to next line at comma (removing trailing spaces)
nmap ,, f,li <Esc>dwi<Enter><Esc>

" =========== PLUGINS =========== "
" if you place them $HOME/.vim then you don't have to load them yourself...
"source $HOME/.vim/plugins/ScrollColor.vim	" allows to scroll thru colour schemes with :SCROLL
"source ~/.vim/plugins/compile.vim                   " compile files: ,; to compile ,,; to run
"source ~/.vim/plugins/josfix.vim                    " after compiling TeX do ,fi to get list of errors, press 'e' to edit
"source ~/.vim/plugin/SpellChecker.vim       "should be done automatically but isn't?!?
"source ~/.vim/tab.vim                       " ,t to align with with word on previous line

"showmarks.vim          " shows your marks (ma, g'a etc)
",mt toggles it on and off.
",ma hides all marks in current buffer
",mm places next available mark.
let showmarks_include="abcdefghijklmnopqrstuvwxyz" " only show marks a--z being the user-set ones.

" ----------------- R PLUGIN ------------- "
"  --- SENDING TO R ---
"  F2 to start R
"  ;rq quit R
"
"  F5 send the entire file to R
"  'r' sends selected bit to R
"  ;l for current line
"  ;bb send current block (be echo)
"  ;cc send current chunk (ce echo)
"  ;pp send current paragraph
"
"  ;rp print the thing under the cursor
"  ;rn names(obj) under the cursor
"  ;ra arguments
"  ;re example
"  ;rh help
"  ;rs summary
"  ;rp plot
"  ;rd set wd to current file path
"
"  --- COMMENTS ---
"  ;xc to comment out a line or selected lines
"  ;xu to uncomment out a line or selected lines
"  ;xx to toggle comment/uncomment
"  ;;     to right-align comments (?)
"
"  --- KNITTING/CHUNKS ---
"  ;kn knit current file
"  gn  next R chunk
"  gN  previous R chunk
"
"  --- OMNI ---
"  C^X C^O omni
"  :RUpdateObjList    updates the omni list with loaded package functions
"
"  --- MISC ---
"  :Rinsert <cmd> pastes the results of <cmd> into the buffer.
"
"  --- HELP ---
"  :Rhelp

" to get the vim-R-plugin working.
let r_syntax_folding=1              " R code folding.
let vimrplugin_map_r=1              " press 'r' with visually selected lines to send to r.
" launch R in Screen window seprate to the editor
"let vimrplugin_screenplugin = 0
"let vimrplugin_tmux = 0
let vimrplugin_assign=0             " don't convert _ to <-, it's annoying.

" split vertically
let vimrplugin_vsplit=1
let g:ScreenImpl = 'Tmux'

" vim-markdown-preview (automatic pandoc conversion on :w or ^P)
"let g:vim_markdown_preview_command = 'pandoc --mathjax --toc -F pandoc-crossref -F pandoc-citeproc -V pagetitle:INFILE --standalone -t html INFILE > OUTFILE'
" this way it's consistent with what RStudio would give me
" title changes away on --standalone...oh well.
let g:vim_markdown_preview_command = 'Rscript -e "library(rmarkdown); invisible(capture.output(suppressMessages(rmarkdown::render(\"INFILE\", output_file=\"OUTFILE\"))))"'
let g:vim_markdown_preview_use_local = 1
let g:vim_markdown_preview_remove_output = 1 
let g:vim_markdown_preview_toggle_hotkey='<C-p>'
let g:vim_markdown_preview_hotkey='<C-k>'

" these have been deprecated
"let vimrplugin_by_vim_instance=1    " each vim instance can open its own R
"let vimrplugin_nosingler = 1        " each vim buffer can open its own R.

" OLD
"let vimrplugin_conqueplugin = 0     " don't use conque
"let vimrplugin_conquevsplit = 1     " vertical split for conque
"let rplugin_term_cmd = "Terminal --title R -e"   " use 'Terminal' (fedora) to open R in...

" RPlugin keybindings:
" F2 starts R
nmap <F2> <Plug>RStart
imap <F2> <Plug>RStart
vmap <F2> <Plug>RStart

" F5 sends the whole file
nmap <F5> <Plug>RSendFile
imap <F5> <Plug>RSendFile

" space sends the current line
nmap <Space> <Plug>RDSendLine

" ============ CONQUE ============ "
"let g:ConqueTerm_CWInsert = 1 " <C-w> commands work in insert mode in Conque
"let g:ConqueTerm_InsertOnEnter = 1 " When switching buffers we go into insert mode

" truncate all lines to 63 characters   :%s/.*/\=printf('%-63.63s',submatch(0))
" disable showmarks it's a pain!
let g:showmarks_enable=0

" ============== DEBUGGING ============== "
" Ctrl-N and Ctrl-P go to next and previous errors in normal mode
nmap <C-n> <esc>:cn<cr>
nmap <C-p> <esc>:cp<cr>

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! %!sudo tee > /dev/null %

" pathogen (any files in the bundle/ folder get installed automagically)
execute pathogen#infect()
Helptags

" ====== vim-pandoc. :help pandoc. ==========
" :Pandoc <format> <options>   add ! to end to open
" :TOC command [open table of contents]
" Hypertext module: press gf to open [local] file linked. gx to open in the
" appropriate application.
" Learn also how to fold pandoc.
let g:pandoc#biblio#use_bibtool=1 " smarter search in omnicomplete
let g:pandoc#biblio#bibs = [expand("~/phd/papers/library.bib")] " bibliography location
let g:pandoc#modules#disabled=["spell"] " turn off spellcheck

" ======= pandoc-syntax ===========
" :help pandoc-syntax-configuration
"let g:pandoc#syntax#conceal#use=0 " turn off conceal
let g:pandoc#syntax#conceal#urls=1 " conceal URLs
" Turn off some conceals I don't like
let g:pandoc#syntax#conceal#blacklist=["atx","codeblock_start","codeblock_delim","image","list","quotes"]

" -- copy paste --
" http://vim.wikia.com/wiki/In_line_copy_and_paste_to_system_clipboard
" Copy-paste to system clip on ubuntu (running Vim in gnome-terminal)
" if xclip < .11: vmap <C-c> y:call system("xclip -i -selection clipboard", getreg("\""))<CR>:call system("xclip -i", getreg("\""))<CR>
vmap <C-c> y: call system("xclip -i -selection clipboard", getreg("\""))<CR>
nmap <C-v> :call setreg("\"",system("xclip -o -selection clipboard"))<CR>p
" lets you Cv in insert mode and keep typing
imap <C-v> <Esc><C-v>a

" Lines added by the Vim-R-plugin command :RpluginConfig (2014-Jul-28 13:47):
"syntax on "syntax on overrides my colours. syntax enable doesn't.

" Use Ctrl+Space to do omnicompletion:
if has("gui_running")
    inoremap <C-Space> <C-x><C-o>
else
    inoremap <Nul> <C-x><C-o>
endif
" Press the space bar to send lines (in Normal mode) and selections to R:
vmap <Space> <Plug>RDSendSelection
nmap <Space> <Plug>RDSendLine

" ======= table-mode (for pandoc tables) =======
" ;tm to toggle table mode (or :TableModeEnable). Reacts/autoformats on |
" https://github.com/dhruvasagar/vim-table-mode/
" or visually select existing and ;tt / :Tableize (CSV separated)
let g:table_mode_corner_corner="+"
let g:table_mode_header_fillchar="="


" ==== TRYING TO USE BUFFERS NOT TABS ==== "
" http://stackoverflow.com/questions/16082991/vim-switching-between-files-rapidly
" :bn, :bp (next, prev), :b[number], :b foo<Tab> (by name tab completion), :ls
" :bd to clos a buffer.
set hidden " avoid the 'unsaved changes' when you navigate b/t buffers
" 'go buffer' -> just press the number of the buffer or name (for tab completion)
nnoremap gb :ls<CR>:b<Space>
" temporarily map :q to :close until I learn to switch to :bd
" cabbrev q <c-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'close' : 'q')<CR>
