%% Matlab startup file

%------------ YAWtb -----------------------------%
olddir=pwd;
cd ~/Copy/matlab/yawtb
yaload;
cd ~/Copy/matlab/SWv1.2.2-svn601
add_all_paths;
cd(olddir);
clear olddir;
%-----------------------------------------------------%

%------------ FreeSurfer -----------------------------%
fshome = getenv('FREESURFER_HOME');
fsmatlab = sprintf('%s/matlab',fshome);
if (exist(fsmatlab) == 7)
    path(path,fsmatlab);
end
clear fshome fsmatlab;
%-----------------------------------------------------%

%------------ FreeSurfer FAST ------------------------%
fsfasthome = getenv('FSFAST_HOME');
fsfasttoolbox = sprintf('%s/toolbox',fsfasthome);
if (exist(fsfasttoolbox) == 7)
    path(path,fsfasttoolbox);
end
clear fsfasthome fsfasttoolbox;
%-----------------------------------------------------%
